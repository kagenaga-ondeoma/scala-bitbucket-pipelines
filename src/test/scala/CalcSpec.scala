import org.scalatest._

class CalcSpec extends FlatSpec with DiagrammedAssertions {

  val calc = new Calc

  "add関数" should "2つの整数の合計値を取得することができる" in {
    assert(calc.add(1, 2) === 3)
  }
}